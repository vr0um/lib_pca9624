/*
 * PCA9624.cpp
 * Copyright (C) 2022 Adrian Badoil 
 *
 * Distributed under terms of the MIT license.
 */

#include "mbed.h"
#include "PCA9624.h"
#include <cstdint>


PCA9624::PCA9624(I2C *i2c, char deviceAddress, const unsigned int p_frequency) {
    _i2c = i2c;
    _writeOpcode = deviceAddress & 0xFE; // low order bit = 0 for write
    _readOpcode  = deviceAddress | 0x01; // low order bit = 1 for read
    _i2c->frequency(p_frequency); // Set the frequency of the I2C interface
    init();
}

char PCA9624::_read(char address) {
    char data[2];

    data[0] = address;
    _i2c->write(_writeOpcode, data, 1);     // Select Register for reading
    _i2c->read(_readOpcode, data, 1);       // Read from selected Register

    return data[0];
}

bool PCA9624::_write(char address, char byte) {
    char data[2];

    data[0] = address;
    data[1] = byte;
    return _i2c->write(_writeOpcode, data, 2) == 0;  // Write data to selected register
}

bool PCA9624::_write_4(char address, char byte_1, char byte_2, char byte_3, char byte_4) {
    char data[5];

    data[0] = address;
    data[1] = byte_1;
    data[2] = byte_2;
    data[3] = byte_3;
    data[4] = byte_4;
    return _i2c->write(_writeOpcode, data, 5) == 0;  // Write data to selected register
}

bool PCA9624::init() {
    bool ret = 0; //return 1 in case of success
    ret = sleep(false);
    if (ret != 1) {
        return ret;
    }
    char state = PWM_EACH_AND_GROUP;
    char data = ((uint8_t)state << 6) | ((uint8_t)state << 4) | ((uint8_t)state << 2) | ((uint8_t)state << 0);
    ret = _write(LEDOUT0, data);
    if (ret != 1) {
        return ret;
    }
    return _write(LEDOUT1, data);
}

bool PCA9624::drive(const char ch, const char vol) {
    char channel = PWM0 + ch;
    return _write(channel, vol);
}

bool PCA9624::drive_4(const char vol_1, const char vol_2, const char vol_3, const char vol_4) {
    return _write_4(PWM0, vol_1, vol_2, vol_3, vol_4);
}

bool PCA9624::sleep(const bool b) {
    char data = 0;
    if (b)
        data = 0b10010001;
    else
        data = 0b10000001;
    return _write(MODE1, data);
}

bool PCA9624::setGroupPWM(const char vol) {
    return _write(GRPPWM, vol);
}

bool PCA9624::reset(){
    char data[2];
    data[0] = 0xA5;
    data[1] = 0x5A;
    return _i2c->write(0x06, data, 2);     // Select Register for reading
}
